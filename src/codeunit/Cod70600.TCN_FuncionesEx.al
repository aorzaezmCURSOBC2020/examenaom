codeunit 70600 "TCN_FuncionesEx"
{
    SingleInstance = true;


    procedure FiltroContieneCodigoF(pFiltro: Text[250]; pCodigo: Text[250]) xSalida: Boolean
    begin
        if pFiltro.Contains(pCodigo) then begin
            xSalida := true;
        end;
    end;

    procedure FiltroConCodigoF(pFiltro: Text[250]; pCodigo: Text[250])
    var
        rlTempCommentLine: Record "Comment Line" temporary;
        xlContiene: Label 'El filtro introducido contiene el código';
        xlNoContiene: Label 'El filtro introducido NO contiene el código';
    begin
        rlTempCommentLine."No." := CopyStr(pFiltro, 1, MaxStrLen(rlTempCommentLine."No."));
        rlTempCommentLine.SetFilter("No.", '%1', UpperCase(pCodigo));
        if rlTempCommentLine.FindFirst() then begin
            Message(xlContiene);
        end else begin
            Error(xlNoContiene);
        end;
    end;
}
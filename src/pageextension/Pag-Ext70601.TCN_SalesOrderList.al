pageextension 70601 "TCN_SalesOrderList" extends "Sales Order List" //MyTargetPageId
{
    actions
    {
        addlast(Documents)
        {
            action(ImportacionDatos)
            {
                Caption = 'Importación pedidos de venta';
                Image = Account;
                trigger OnAction()
                begin
                    pFichaEntradaDatos.SetValoresF(1, 'Num empledo', 'Introduzca aquí el número del empleado');
                    pFichaEntradaDatos.SetValoresF(2, 'Separador', 'Introduzca el separador');
                    if pFichaEntradaDatos.RunModal() in [Action::LookupOK, Action::OK] then begin
                        pFichaEntradaDatos.GetValoresF(1, xNumEmp);
                    end;
                    // Aquí ya tengo el xNumEmp y xSeparador en mis variables globales
                    // Hacemos función para importar datos
                    ImportacionPedVentaF(xNumEmp, xSeparador);
                end;
            }
            action(PruebaEjer3)
            {
                Caption = 'Filtro contiene cadena';
                trigger OnAction()
                var
                    xlContiene: Label 'El filtro introducido contiene el código';
                    xlNoContiene: Label 'El filtro introducido NO contiene el código';
                begin
                    pFichaEntradaDatos.SetValoresF(1, 'Filtro', 'filtro');
                    pFichaEntradaDatos.SetValoresF(2, 'Código', 'codigo');
                    if pFichaEntradaDatos.RunModal() in [Action::LookupOK, Action::OK] then begin
                        pFichaEntradaDatos.GetValoresF(1, xFiltro);
                        pFichaEntradaDatos.GetValoresF(2, xCodigo);
                        if cuFuncionesEx.FiltroContieneCodigoF(xFiltro, xCodigo) then begin
                            Message(xlContiene);
                        end else begin
                            Error(xlNoContiene);
                        end;
                        Clear(pFichaEntradaDatos);
                    end;
                end;
            }
            action(Prueba2Ejer3)
            {
                Caption = 'Filtro contiene cadena 2';
                trigger OnAction()
                var
                    xlContiene: Label 'El filtro introducido contiene el código';
                    xlNoContiene: Label 'El filtro introducido NO contiene el código';
                begin
                    pFichaEntradaDatos.SetValoresF(1, 'Filtro', 'filtro');
                    pFichaEntradaDatos.SetValoresF(2, 'Código', 'codigo');
                    if pFichaEntradaDatos.RunModal() in [Action::LookupOK, Action::OK] then begin
                        pFichaEntradaDatos.GetValoresF(1, xFiltro);
                        pFichaEntradaDatos.GetValoresF(2, xCodigo);
                        cuFuncionesEx.FiltroConCodigoF(xFiltro, xCodigo);
                        Clear(pFichaEntradaDatos);
                    end;
                end;
            }
        }
    }
    procedure ImportacionPedVentaF(pNumCliente: Text[250]; pSeparador: Text[250])
    var
        rlTempTempBlob: Record TempBlob;
        xlInStream: InStream;
        xlFichero: Text;
        xlLinea: Text;
        xlNumLinea: Integer;
        rlTempCommentLine: Record "Comment Line" temporary;
        rlSalesLine: Record "Sales Line";
        rlSalesHeader: Record "Sales Header";
        xlCampo: Text;
        xlNumCampo: Integer;
        xlErrorCargar: Label 'Error al cargar el fichero, vuelve a intentarlo';
    begin
        rlTempTempBlob.Blob.CreateInStream(xlInStream, TextEncoding::Windows);
        if UploadIntoStream('Seleccione el fichero', '', 'Archivos de texto (*.txt, *.csv)|*.txt;*.csv|Todos los archivos (*.*)|*.*', xlFichero, xlInStream) then begin
            while not xlInStream.EOS do begin
                xlInStream.ReadText(xlLinea);
                xlNumLinea += 1;
                xlNumCampo := 0;
                foreach xlCampo in xlLinea.Split(pSeparador) do begin
                    xlNumCampo += 1;
                    if xlCampo = 'C' then begin
                        xlNumCampo += 1;
                        Init();
                        if InsertarCabecera(xlNumCampo, xlCampo) then begin
                            Insert(true);
                        end else begin
                            rlTempCommentLine.Init();
                            rlTempCommentLine."Line No." := xlNumLinea;
                            rlTempCommentLine.Comment := CopyStr(StrSubstNo('Error en cabecera: %1', GetLastErrorText),
                                                                        1,
                                                                        MaxStrLen(rlTempCommentLine.Comment));
                            rlTempCommentLine.Insert(false);
                        end;
                        xlNumLinea += 1;
                        xlInStream.ReadText(xlLinea);
                        foreach xlCampo in xlLinea.Split(pSeparador) do begin
                            xlNumCampo += 1;
                            Init();
                            if InsertarLinea(xlNumCampo, xlCampo, xNumDoc) then begin
                                Insert(true);
                            end else begin
                                rlTempCommentLine.Init();
                                rlTempCommentLine."Line No." := xlNumLinea;
                                rlTempCommentLine.Comment := CopyStr(StrSubstNo('Error en línea: %1', GetLastErrorText),
                                                                            1,
                                                                            MaxStrLen(rlTempCommentLine.Comment));
                                rlTempCommentLine.Insert(false);
                            end;
                            xlNumLinea += 1;
                        end;
                    end;
                end;
            end;
            if rlTempCommentLine."Line No." = 0 then begin
                Message('Proceso finalizado');
            end else begin
                Page.Run(0, rlTempCommentLine);
            end;
        end else begin
            Message(xlErrorCargar);
        end;
    end;

    local procedure CalcularFechaF(var pFecha: Text) xSalida: Date
    var
        xlDia: Integer;
        xlMes: Integer;
        xlAño: Integer;
    begin
        Evaluate(xlDia, CopyStr(pFecha, 1, 2));
        Evaluate(xlMes, CopyStr(pFecha, 3, 2));
        Evaluate(xlAño, CopyStr(pFecha, 5, 4));
        xSalida := DWY2Date(xlDia, xlMes, "xlAño");
    end;

    [TryFunction]
    local procedure InsertarCabecera(pNumCampo: Integer; xlCampo: Text)
    var
        rlSalesHeader: Record "Sales Header";
    begin
        rlSalesHeader.Validate("No.", '');
        // Para que me asigne automáticamente el número del doc en el onvalidate
        xNumDoc := rlSalesHeader."No.";
        case pNumCampo of
            2:
                begin
                    rlSalesHeader.Validate("Posting Date", CalcularFechaF(xlCampo));
                end;
            4:
                begin
                    Evaluate(rlSalesHeader."Document Date", xlCampo);
                    rlSalesHeader.Validate(rlSalesHeader."Document Date");
                end;
            5:
                begin
                    Evaluate(rlSalesHeader."External Document No.", xlCampo);
                    rlSalesHeader.Validate(rlSalesHeader."External Document No.");
                end;
        end;
    end;

    [TryFunction]
    local procedure InsertarLinea(pNumCampo: Integer; pCampo: Text; pDocNum: Code[20])
    var
        rlSalesLine: Record "Sales Line";
    begin
        rlSalesLine.Validate("Document No.", pDocNum);
        case pNumCampo of
            2:
                begin
                    case UpperCase(pCampo) of
                        'C':
                            begin
                                rlSalesLine.Validate(Type, rlSalesLine.Type::"G/L Account");
                            end;
                        'P':
                            begin
                                rlSalesLine.Validate(Type, rlSalesLine.Type::Item);
                            end;
                    end;
                end;
            3:
                begin
                    Evaluate(rlSalesLine."No.", pCampo);
                    rlSalesLine.Validate("No.");
                end;
            4:
                begin
                    Evaluate(rlSalesLine.Description, pCampo);
                    rlSalesLine.Validate(Description);
                end;
            5:
                begin
                    Evaluate(rlSalesLine.Quantity, pCampo);
                    rlSalesLine.Validate(Quantity, rlSalesLine.Quantity / 100);
                end;
            6:
                begin
                    Evaluate(rlSalesLine."Unit Cost", pCampo);
                    rlSalesLine.Validate("Unit Cost", rlSalesLine."Unit Cost" / 100);
                end;
            7:
                begin
                    Evaluate(rlSalesLine."Line Discount %", pCampo);
                    rlSalesLine.Validate("Line Discount %", rlSalesLine."Line Discount %" / 100);
                end;
        end;
    end;

    var
        cuFuncionesEx: Codeunit TCN_FuncionesEx;
        pFichaEntradaDatos: Page TCN_FichaEntradaDatos;
        xNumEmp: Text;
        xSeparador: Text;
        xNumDoc: Code[20];
        xFiltro: Text[250];
        xCodigo: Text[250];
}
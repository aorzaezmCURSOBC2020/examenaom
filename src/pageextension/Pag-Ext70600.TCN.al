pageextension 70600 "TCN" extends "Customer List" //Prueba de set y get de datos
{
    layout
    {

    }

    actions
    {
        addlast(Creation)
        {
            action(BotonPrueba)
            {
                Caption = 'Prueba examen';
                trigger OnAction()
                begin
                    LlamarPaginaF();
                end;
            }
            action(SacoDato)
            {
                trigger OnAction()
                begin
                    RecogerDatosF();
                end;
            }
        }
    }
    local procedure LlamarPaginaF()
    var
        lFecha: Date;
    begin
        pFichaEntradaDatos.SetValoresF(1, 'Num cliente', '10000');
        pFichaEntradaDatos.SetValoresF(2, 'Nombre cliente', 'Alejandro');
        pFichaEntradaDatos.SetValoresF(1, 'Prueba decimal 1', 2.33);
        pFichaEntradaDatos.SetValoresF(2, 'Prueba decimal 2', 5.23);
        pFichaEntradaDatos.SetValoresF(1, 'Prueba fecha', lFecha);
        pFichaEntradaDatos.RunModal();
    end;

    local procedure RecogerDatosF()
    var

    begin
        pFichaEntradaDatos.GetValoresF(1, xText01);
        pFichaEntradaDatos.GetValoresF(2, xText02);
        pFichaEntradaDatos.GetValoresF(1, xDecimal01);
        pFichaEntradaDatos.GetValoresF(2, xDecimal02);
        pFichaEntradaDatos.GetValoresF(1, xDate01);
        pFichaEntradaDatos.GetValoresF(2, xDate02);
        Message('%1, %2, %3, %4, %5, %6', xText01, xText02, xDecimal01, xDecimal02, xDate01, xDate02);
    end;

    var
        pFichaEntradaDatos: Page TCN_FichaEntradaDatos;
        xText01: Text;
        xText02: Text;
        xDecimal01: Decimal;
        xDecimal02: Decimal;
        xDate01: Date;
        xDate02: Date;
}
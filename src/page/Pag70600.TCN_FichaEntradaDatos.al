page 70600 "TCN_FichaEntradaDatos"
{
    Caption = 'Lista de entrada de datos';
    PageType = Card;
    ApplicationArea = All;
    UsageCategory = Documents;
    SourceTable = TCN_EntradaTexto;

    layout
    {
        area(Content)
        {
            group(Campos)
            {
                field(Txt01; xTextoArray[1])
                {
                    ApplicationArea = All;
                    Visible = xVisible01;
                    CaptionClass = StrSubstNo('3, %1', xCaptionArray[1]);
                }
                field(Txt02; xTextoArray[2])
                {
                    ApplicationArea = All;
                    Visible = xVisible02;
                    CaptionClass = StrSubstNo('3, %1', xCaptionArray[2]);
                }
                field(Num01; xDecimalArray[1])
                {
                    ApplicationArea = All;
                    Visible = xVisible03;
                    CaptionClass = StrSubstNo('3, %1', xCaptionArray[3]);
                }
                field(Num02; xDecimalArray[2])
                {
                    ApplicationArea = All;
                    Visible = xVisible04;
                    CaptionClass = StrSubstNo('3, %1', xCaptionArray[4]);
                }
                field(Date01; xDateArray[1])
                {
                    ApplicationArea = All;
                    Visible = xVisible05;
                    CaptionClass = StrSubstNo('3, %1', xCaptionArray[5]);
                }
                field(Date02; xDateArray[2])
                {
                    ApplicationArea = All;
                    Visible = xVisible06;
                    CaptionClass = StrSubstNo('3, %1', xCaptionArray[6]);
                }
            }
        }
    }

    trigger OnOpenPage()
    var
        xlContador: Integer;
    begin
        // cuFuncionesEx.GetParametrosF(xVisible01, xVisible02, xVisible03, xVisible04, xVisible05, xVisible05);
        // GetValoresF();

    end;

    var
        xVisible01: Boolean;
        xVisible02: Boolean;
        xVisible03: Boolean;
        xVisible04: Boolean;
        xVisible05: Boolean;
        xVisible06: Boolean;
        // cuFuncionesEx: Codeunit TCN_FuncionesEx;
        xCaptionArray: array[6] of Text;
        xTextoArray: array[2] of Text;
        xDecimalArray: array[2] of Decimal;
        xDateArray: array[2] of Date;

    procedure SetValoresF(pContador: Integer; pCaption: Text; pTexto: Text)
    var
    begin
        case pContador of
            1:
                begin
                    xCaptionArray[1] := pCaption;
                    xTextoArray[1] := pTexto;
                    xVisible01 := true;
                end;
            2:
                begin
                    xCaptionArray[2] := pCaption;
                    xTextoArray[2] := pTexto;
                    xVisible02 := true;
                end;
        end;
    end;

    procedure SetValoresF(pContador: Integer; pCaption: Text; pNum: Decimal)
    begin
        case pContador of
            1:
                begin
                    xCaptionArray[3] := pCaption;
                    xDecimalArray[1] := pNum;
                    xVisible03 := true;
                end;
            2:
                begin
                    xCaptionArray[4] := pCaption;
                    xDecimalArray[2] := pNum;
                    xVisible04 := true;
                end;
        end;
    end;

    procedure SetValoresF(pContador: Integer; pCaption: Text; pDate: Date)
    var
    begin
        case pContador of
            1:
                begin
                    xCaptionArray[5] := pCaption;
                    xDateArray[1] := pDate;
                    xVisible05 := true;
                end;
            2:
                begin
                    xCaptionArray[6] := pCaption;
                    xDateArray[2] := pDate;
                    xVisible06 := true;
                end;
        end;
    end;

    procedure GetValoresF(pContador: Integer; var pTexto: Text)
    begin
        case pContador of
            1:
                begin
                    pTexto := xTextoArray[1];
                end;
            2:
                begin
                    pTexto := xTextoArray[2];
                end;
        end;
    end;

    procedure GetValoresF(pContador: Integer; var pDecimal: Decimal)
    begin
        case pContador of
            1:
                begin
                    pDecimal := xDecimalArray[1];
                end;
            2:
                begin
                    pDecimal := xDecimalArray[2];
                end;
        end;
    end;

    procedure GetValoresF(pContador: Integer; var pFecha: Date)
    begin
        case pContador of
            1:
                begin
                    pFecha := xDateArray[1];
                end;
            2:
                begin
                    pFecha := xDateArray[2];
                end;
        end;
    end;
}
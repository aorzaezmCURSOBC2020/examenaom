table 70600 "TCN_EntradaTexto"
{
    DataClassification = ToBeClassified;

    fields
    {
        field(1; Id; Integer)
        {
            Caption = 'Id';
            DataClassification = ToBeClassified;
        }
        field(2; Txt01; Text[250])
        {
            Caption = 'Texto 01';
            DataClassification = ToBeClassified;
        }
        field(3; Txt02; Text[250])
        {
            Caption = 'Texto 02';
            DataClassification = ToBeClassified;
        }
        field(4; Num01; Decimal)
        {
            Caption = 'Número 01';
            DataClassification = ToBeClassified;
        }
        field(5; Num02; Decimal)
        {
            Caption = 'Número 02';
            DataClassification = ToBeClassified;
        }
        field(6; Date01; Date)
        {
            Caption = 'Fecha 01';
            DataClassification = ToBeClassified;
        }
        field(7; Date02; Date)
        {
            Caption = 'Fecha 02';
            DataClassification = ToBeClassified;
        }
    }

    keys
    {
        key(PK; Id)
        {
            Clustered = true;
        }
    }
}